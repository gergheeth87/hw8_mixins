from django.contrib.auth.mixins import AccessMixin
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect
from django.db import models


class AuthorAndElapsedTimeRequiredMixin(AccessMixin):
    """
    Deny a request with a permission error if the test_func() method returns
    False.
    """

    request_date = None
    allowed_days = None

    def test_func(self):
        """
        Returns True if the user is the author and
        no more than the specified number of days have passed since publication
        """
        time_diff = self.request_date - self.get_object().published_date
        t_days = time_diff.days
        return self.get_object().author == self.request.user and t_days <= self.allowed_days

    def dispatch(self, request, *args, **kwargs):
        user_test_result = self.test_func()
        if not user_test_result:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class CheckElapsedTimeMixin(AccessMixin):
    """
    Deny a request with a permission error if the test_func() method returns
    False.
    """

    request_date = None
    allowed_days = None

    def test_func(self):
        """
        Returns True if no more than the specified number of days have passed since publication
        """
        time_diff = self.request_date - self.get_object().published_date
        t_days = time_diff.days
        return t_days == self.allowed_days

    def dispatch(self, request, *args, **kwargs):
        user_test_result = self.test_func()
        if not user_test_result:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class AuthorRequiredMixin(AccessMixin):
    """
    Deny a request with a permission error if the test_func() method returns
    False.
    """

    def test_func(self):
        """
        Returns True if the user is the author of publication
        """
        return self.get_object().author == self.request.user

    def dispatch(self, request, *args, **kwargs):
        user_test_result = self.test_func()
        if not user_test_result:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class ActiveUserRequiredMixin(AccessMixin):
    """
    Deny a request with a permission error if the test_func() method returns
    False.
    """

    def test_func(self):
        """
        Returns True if the user is active
        """
        return self.request.user.is_active

    def dispatch(self, request, *args, **kwargs):
        user_test_result = self.test_func()
        if not user_test_result:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class SuperuserRequiredMixin(AccessMixin):
    """
    Deny a request with a permission error if the test_func() method returns
    False.
    """

    def test_func(self):
        """
        Returns True if the user is superuser
        """
        return self.request.user.is_superuser

    def dispatch(self, request, *args, **kwargs):
        user_test_result = self.test_func()
        if not user_test_result:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class RedirectIfSuperuserdMixin:
    """
    Redirect if the user is superuser
    """

    redirect_to = None

    def get_redirect_url(self):
        if not self.redirect_to:
            raise ImproperlyConfigured('no url to redirect to. please specify a redirect url')
        return str(self.redirect_to)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return HttpResponseRedirect(self.get_redirect_url())
        return super().dispatch(request, *args, **kwargs)


class RedirectIfUserIsActiveMixin:
    """
    Redirect if the user is active
    """

    redirect_to = None

    def get_redirect_url(self):
        if not self.redirect_to:
            raise ImproperlyConfigured('no url to redirect to. please specify a redirect url')
        return str(self.redirect_to)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_active:
            return HttpResponseRedirect(self.get_redirect_url())
        return super().dispatch(request, *args, **kwargs)


class RedirectIfUserIsAuthorMixin:
    """
    Redirect if the user is author
    """

    redirect_to = None

    def get_redirect_url(self):
        if not self.redirect_to:
            raise ImproperlyConfigured('no url to redirect to. please specify a redirect url')
        return str(self.redirect_to)

    def dispatch(self, request, *args, **kwargs):
        if self.get_object().author == self.request.user:
            return HttpResponseRedirect(self.get_redirect_url())
        return super().dispatch(request, *args, **kwargs)


class TimeStampCreatedAtMixin(models.Model):
    """
    Inherit in the model in order to get a field with the creation time in it
    """
    created_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class TimeStampUpdatedAtMixin(models.Model):
    """
    Inherit in the model in order to get a field with the update time in it
    """
    updated_time = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

