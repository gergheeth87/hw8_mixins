from django import forms
from .models import Chat, AddChatUsers


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class ChatForm(forms.ModelForm):
    message_text = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Chat
        fields = ('message_text', )


class AddChatUsersForm(forms.ModelForm):
    add_active_users = forms.BooleanField(required=False)

    class Meta:
        model = AddChatUsers
        fields = ('add_active_users', )
