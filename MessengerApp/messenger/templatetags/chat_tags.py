from django import template

register = template.Library()


@register.simple_tag
def has_permission(user, permission):
    return user.has_perm(permission)
